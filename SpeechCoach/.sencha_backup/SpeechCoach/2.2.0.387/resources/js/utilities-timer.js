function durationDisplayForSecs(durationInSecs)
{ 
   var temp = Number(durationInSecs)*1000;
   return durationDisplayForMilliSecs(temp);
    
} 

function durationDisplayForMilliSecs(durationInMilliSecs)
{
    //convert millsecs in readable property

var intMilliSeconds = Number(durationInMilliSecs);
var intSeconds = intMilliSeconds/1000
var intMinutes = parseInt(intSeconds / 60);


intSeconds = intSeconds % 60;

var singleSecondCheck =  parseInt(intSeconds);

intSeconds = intSeconds.toFixed(1);//round up secs to 1 dec places


var singleMinuteCheck = intMinutes;

var secsDisplay;
var minsDisplay;

//Check for single digit and padd with zero
if(singleSecondCheck.toString().length == 1)
{
    secsDisplay =  '0' + intSeconds.toString();
}
else
{
    secsDisplay = intSeconds.toString();
}


if(singleMinuteCheck.toString().length == 1)
{
    minsDisplay =  '0' + intMinutes.toString();
}
else
{
    minsDisplay =  intMinutes.toString();
}


if(durationInMilliSecs=='')
{
    return '00:00.0';
}
else if (durationInMilliSecs=='0')
{
    return '00:00.0';
}
else
{

    return minsDisplay + ':' + secsDisplay ;
}


}


function maxDurationDisplayForSlider(durationInSecs)
{ 
    var temp = Number(durationInSecs)*1000;
    var intMilliSeconds = Number(temp);
    var intSeconds = intMilliSeconds/1000

    var intMinutes = parseInt(intSeconds / 60);
    return intMinutes + ' mins';
}
