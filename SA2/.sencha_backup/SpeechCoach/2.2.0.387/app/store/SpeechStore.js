/*
 * File: app/store/SpeechStore.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('SpeechCoach.store.SpeechStore', {
    extend: 'Ext.data.Store',

    requires: [
        'SpeechCoach.model.Speech'
    ],

    config: {
        autoLoad: true,
        autoSync: true,
        data: [
            {
                name: '1-2 mins',
                greenLight: '60',
                yellowLight: '90',
                redLight: '120'
            },
            {
                name: '2-3 mins',
                greenLight: '120',
                yellowLight: '150',
                redLight: '180'
            },
            {
                name: '3-4 mins',
                greenLight: '180',
                yellowLight: '210',
                redLight: '240'
            },
            {
                name: '3-5 mins',
                greenLight: '180',
                yellowLight: '240',
                redLight: '300'
            },
            {
                name: '4-6 mins',
                greenLight: '240',
                yellowLight: '300',
                redLight: '360'
            },
            {
                name: '5-7 mins',
                greenLight: '300',
                yellowLight: '360',
                redLight: '420'
            },
            {
                name: '6-8 mins',
                greenLight: '360',
                yellowLight: '420',
                redLight: '480'
            },
            {
                name: '8-10 mins',
                greenLight: '480',
                yellowLight: '540',
                redLight: '600'
            },
            {
                name: '10-12 mins',
                greenLight: '600',
                yellowLight: '660',
                redLight: '720'
            },
            {
                name: '11-15 mins',
                greenLight: '660',
                yellowLight: '780',
                redLight: '900'
            },
            {
                name: '13-15 mins',
                greenLight: '780',
                yellowLight: '840',
                redLight: '900'
            },
            {
                name: '14-18 mins',
                greenLight: '840',
                yellowLight: '960',
                redLight: '1080'
            },
            {
                name: '15-20 mins',
                greenLight: '900',
                yellowLight: '1050',
                redLight: '1200'
            },
            {
                name: '20-30 mins',
                greenLight: '1200',
                yellowLight: '1500 ',
                redLight: '1800'
            }
        ],
        model: 'SpeechCoach.model.Speech',
        storeId: 'SpeechStore',
        proxy: {
            type: 'localstorage',
            id: 'SpeechStore'
        }
    }
});