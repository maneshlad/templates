/*
 * File: app/view/MenuView.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('TemplateApp.view.MenuView', {
    extend: 'Ext.Container',
    alias: 'widget.MenuView',

    config: {
        open: false,
        bottom: 0,
        cls: 'mainmenu',
        docked: 'left',
        left: 0,
        padding: '97 0 0 0',
        top: 0,
        width: 266,
        zIndex: 0,
        layout: {
            type: 'fit'
        },
        scrollable: 'vertical',
        items: [
            {
                xtype: 'titlebar',
                docked: 'top',
                hidden: false,
                title: 'AppName',
                layout: {
                    align: 'center',
                    type: 'hbox'
                }
            },
            {
                xtype: 'list',
                name: 'menuList',
                itemTpl: [
                    '<small class="muted"> ',
                    '    <div style="float: left">{text}',
                    '    </div><div style="float: right">&nbsp;&nbsp;&nbsp;&nbsp;</div>',
                    ' </small>',
                    '',
                    '',
                    ''
                ],
                store: 'MenuStore',
                grouped: true,
                onItemDisclosure: true,
                striped: true,
                variableHeights: true
            }
        ]
    },

    toggle: function() {
        this.setOpen(!this.getOpen());
    },

    updateOpen: function(open) {
        var targetEl;
        var parentCt = this.up();

        if (!parentCt) {
            return;
        }

        targetEl = parentCt.innerElement;

        if (open) {
            targetEl.translate(this.getWidth(), 0, 0);
            this.maskCmp.show();   
        }
        else {
            targetEl.translate(0, 0, 0);
            this.maskCmp.hide();    
        }
    },

    onDestroy: function() {
        this.maskCmp.destroy();   
        delete this.maskCmp; 
        this.callParent(arguments);
    },

    onMaskRelease: function() {
        this.setOpen(false);
    },

    initialize: function() {
        this.callParent();


        var viewport = Ext.Viewport;
        this.maskCmp = viewport.add({
            xtype   : 'component',
            cls     : 'mainmenu-mask',
            top     : 0,
            zIndex  : 5000,
            hidden  : true,
            width   : 9999,
            left    : this.getWidth(),
            bottom  : 0  
        });

        this.maskCmp.element.on({    
            scope   : this,
            touchend: 'onMaskRelease'        
        });	
    }

});